# CI snippet collection

This repo shall be a collection of reusable CI snippets. They can be included
into other `.gitlab-ci.yml`'s via the `include` directive. For further details
on this, see: [Gitlab CI yml - include](https://docs.gitlab.com/ee/ci/yaml/#include).

**TL;DR:**
````yml
include: 'https://gitlab.com/${CI_PROJECT_NAMESPACE}/ci-snippets/-/raw/master/.ssh-remote-debugging.yml'
````
includes

````yml
before_script:
  - apt-get update -qq && apt-get install -y -qq sqlite3 libsqlite3-dev nodejs
  - gem install bundler --no-ri --no-rdoc
  - bundle install --jobs $(nproc) "${FLAGS[@]}"
````
in your CI yml.

*Note:*
[Gitlab CI yml - anchors](https://docs.gitlab.com/ee/ci/yaml/#anchors) might
be also interesting for you.